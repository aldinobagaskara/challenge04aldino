class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <img src= ${this.image} class="img carscard-images"> 
    <p class="cars-title"> ${this.manufacture} / ${this.model} </p>
    <p class="cars-price"> <strong> Rp ${this.rentPerDay} / hari </strong></p>
    <p class="cars-subtitle">${(this.description !== "")?this.description:'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, tenetur!'}</p>
    <div class="cars-detail"><img src="./images/fi_users.png"> ${this.capacity} </div>
    <div class="cars-detail"><img src="./images/fi_settings.png"> ${this.transmission} </div>
    <div class="cars-detail"><img src="./images/fi_calendar.png"> ${this.year} </div>
    <div class="col-12 buttonRow">
      <div class="d-grid gap-2">
      <button type="button" class="btn btn-secondary btn-sm chooseButton"> Pilih Mobil </button>
      </div>
    </div>
    `;
  }

}
